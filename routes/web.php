<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('/','control@index');
Route::get('/login','control@login');
Route::get('/registro','control@registro');
Route::get('/recargar','control@recargar');



/*
Route::get('/personas/{id}', function ($id) {
    return View::make('prueba')->with('entrada',$id);
});
*/

Route::get('/personas/{id}','control@segunda');
Route::get('/subasta', function () {
	
    return View::make('subasta');



});
Route::get('/subasta2', function () {
	
    return View::make('subasta2');



});
Route::get('/terminadas', function () {
	if (Session::has('correo'))
{
    return View::make('terminadas');
}else{
	    return View::make('login');

}
});


Route::get('/registrarsubasta', function () {
		if (Session::has('correo'))
{
return View::make('registrarsu')->with('id',session()->get('correo'));
}else{
	    return View::make('login');

}
});
/*
Route::get('/videos/{numero}','control@primera');
*/

Route::get('/sesion',function(){

$r=Session::get('correo');
echo $r;

});




Route::post('/registrar','control@inicio');
Route::get('/subasta/{precio}/{imagen}','control@agregarsub');
Route::get('/perfil','control@perfil');
Route::post('/listar','control@listar');
Route::post('/imagens','control@nova');
Route::get('/productos','control@productos');
Route::post('/agregar','control@agregar');
Route::post('/ofertar','control@ofertar');
Route::post('/getofertar','control@getofertar');
Route::get('/salir','control@salir');
Route::post('/aceptar','control@aceptar');
Route::post('/rechazar','control@rechazar');
Route::post('/agregar','control@agregar');
Route::post('/crear','control@crear');
Route::get('/gettransaccion','control@gettransaccion');
Route::get('/pagar','control@pagar');
Route::get('/cobrar','control@cobrar');
Route::post('/pagar','control@pagar');
Route::post('/imagen','control@imagen');
Route::post('/modificar','control@modificar');
Route::post('/cambiarpass','control@pass');
Route::post('/registrarp','control@registrarp');





?>






<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class producto extends Model
{
	protected $table='producto';
	public $timestamps = false;


    public function scopelistar($query,$id){
    return $query->join('persona_producto','persona_producto.id_producto','=','producto.id')->select('producto.*')->where('persona_producto.id_persona',$id)->where('producto.estado',1);
    }
    public function scopelistarproducto($query,$id){
    return $query->where('id',$id);

    }


    //
}

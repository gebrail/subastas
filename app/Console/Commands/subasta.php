<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class subasta extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subasta:su';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $carbon = new \Carbon\Carbon();
        $date = $carbon->now();
       /* $array[]=['id_subasta'=>$id,'id_comprador'=>session()->get('correo'),'valor'=>$precio,'fecha'=>$date];
        DB::table('transaccion')->insert($array); */
        DB::table('subasta')
            ->where('fecha_final','<',$date)
            ->update(['estado' => 0]);


         $this->info('All inactive users are deleted successfully!');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ofertar extends Model
{
protected $table='ofertar';
public $timestamps = false;
   public $incrementing = false;


public function scopecantidad($query,$idsubasta,$persona){
	$r= $query->where('id_comprador',$persona)->where('id_subasta',$idsubasta)->get()->count();	
	$r2[]=$r;
	return $r2;
}
public function scopeagregar($query,$idsu,$idco,$precio,$fecha){
	$ofertar = new ofertar;
	$ofertar->id_subasta=$idsu;
	$ofertar->id_comprador=$idco;
	$ofertar->valor=$precio;
	$ofertar->fecha=$fecha;
	$ofertar->save();
}
public function scopetraeroferta($query,$subasta,$comprador){
return $query->where('id_comprador',$comprador)->where('id_subasta',$subasta);
}
public function scopeactualizarprecio($query,$id,$idsubasta,$precio){
return $query->where('id_comprador',$id)->where('id_subasta',$idsubasta)
->update(['valor'=>$precio]);
    
}

   
}

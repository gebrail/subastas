<!DOCTYPE html>
<html ng-app="myApp" lang="en">
<head>
<meta charset="utf-8">
<title>Topi-Subastas</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Bootstrap 3 template for corporate business" />
<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/cubeportfolio.min.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />


<!-- Theme skin -->
<link id="t-colors" href="skins/default.css" rel="stylesheet" />

	<!-- boxed bg -->
	<link id="bodybg" href="bodybg/bg1.css" rel="stylesheet" type="text/css" />

<!-- =======================================================
    Theme Name: Sailor
    Theme URL: https://bootstrapmade.com/sailor-free-bootstrap-theme/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
======================================================= -->
</head>
<body>



<div id="wrapper">
	<!-- start header -->
	<header Style = "background-color : white">
		
				
					
						
					
						<div id="sb-search" >
							<form>
								
								<input class="sb-search-input"  /> 
								
							</form>
						</div>
       
	</header>
	<!-- end header -->
	
	<section id="content" >
<div class="container" >

<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
        <div ng-controller="submitController">

		<form name="form">
			
			
							
						
			
			<h2 align="center" Style = "color:#DA0404"> <img  src="img/LOGO_TOPICOS.png" alt="" width="80" height="80" border="0" />  Iniciar sesión </h2>
			<hr class="colorgraph">

			<div class="form-group">
				<input  type="email" name="email" id="email" class="form-control input-lg" placeholder="Email" tabindex="10" ng-model="activos.nombre" ng-required="activos.nombre">
			</div>
			<div class="form-group">
				<input type="password" class="form-control input-lg" id="exampleInputPassword1" placeholder="Contraseña" ng-model="activos.contra"  ng-required="activos.contra" >
			</div>

			<div class="row">
				<div class="col-xs-4 col-sm-3 col-md-3">
					<span class="button-checkbox">
						<button type="button" class="btn" data-color="info" tabindex="7">Recordar</button>
                        <input type="checkbox" name="t_and_c" id="t_and_cs" class="hidden" value="1" />
					</span>
				</div>
			</div>
			
			<hr class="colorgraph">
			<div class="row">
				<div class="col-xs-12 col-md-6"><input Style = "background-color:#DA0404 ; opacity:0.7" type="submit" value="Ingresar" class="btn btn-primary btn-block btn-lg" tabindex="7" ng-click="ingresar(activos);"></div>
				<div class="col-xs-12 col-md-6">¿No te encuentras registrado? <a href="/registro">Registrate</a></div>
			</div>
		</form>
		</div>
	</div>
</div>

</div>
	</section>

	<footer Style = "background-color : white">
	
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				
					<div class="copyright">
						<p>&copy; Sailor Theme - All Right Reserved</p>
                        <div class="credits">
                            
                            <a href="https://bootstrapmade.com/free-business-bootstrap-themes-website-templates/">Business Bootstrap Themes</a> by <a href="https://bootstrapmade.com/">BootstrapMade</a>
						                     
					   </div>
						
					</div>
				 
				
			</div>
		</div>
	</div>
	</footer>
	

<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/stellar.js"></script>
<script src="js/classie.js"></script>
<script src="js/uisearch.js"></script>
<script src="js/jquery.cubeportfolio.min.js"></script>
<script src="js/google-code-prettify/prettify.js"></script>
<script src="js/animate.js"></script>
<script src="js/custom.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.3/angular-route.js"></script>
<script src="js/script2.js"></script>
<script data-require="angular-ui-bootstrap@0.3.0" data-semver="0.3.0" src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.3.0.min.js"></script>


	
</body>
</html>
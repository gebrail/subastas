<!DOCTYPE html>
<html ng-app="myApp" lang="en">
<head>
<meta charset="utf-8">
<title>Topi-Subastas</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Bootstrap 3 template for corporate business" />
<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="plugins/flexslider/flexslider.css" rel="stylesheet" media="screen" />	
<link href="css/cubeportfolio.min.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />

<!-- Theme skin -->
<link id="t-colors" href="skins/default.css" rel="stylesheet" />

	<!-- boxed bg -->
	<link id="bodybg" href="bodybg/bg1.css" rel="stylesheet" type="text/css" />

<!-- =======================================================
    Theme Name: Sailor
    Theme URL: https://bootstrapmade.com/sailor-free-bootstrap-theme/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
======================================================= -->

</head>
<body>



<div id="wrapper">
	<!-- start header -->
	   <header>
  <div class="top">
        <div class="container">
          <div Style = "floar:center" class="row" ng-controller="submitController">
            <div class="col-md-4">
              
                <input Style = "width: 300px" type="email" name="email" id="email" class="form-control input-lg" placeholder="Email" ng-model="activos.nombre">
              
            </div>
          
            <div class="col-md-4">
                <input Style = "width: 300px" type="password" class="form-control input-lg" id="exampleInputPassword1" placeholder="Contraseña" ng-model="activos.contra" >
                                    <div class="col-xs-12 col-md-6">¿No te encuentras registrado? <a href="/registro">Registrate</a></div>

            </div>

            
            <div class="col-md-4">
              <input Style = "background-color:#DA0404 ; opacity:0.7" type="submit" value="Ingresar" class="btn btn-primary btn-block btn-lg" tabindex="7" ng-click="ingresar(activos);">
            </div>
          
          </div>
        </div>
      </div>  
      
      
        <div class="navbar navbar-default navbar-static-top" style = "background-color:#DA0404">
            <div class="container" >
                <div class="navbar-header" >
                    <button   type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="img/LOGO_TOPICOS.png" alt="" width="76" height="50" /></a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li ><a Style = "color: white" href="/">INICIO</a></li>
                        
                        <li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">SUBASTAS<i class="fa fa-angle-down"></i></a>
              <ul class="dropdown-menu">
                                <li><a href="/subasta2">VER SUBASTAS</a></li>
                                
                
                            </ul>
            </li>
                        <li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">REGISTRO<i class="fa fa-angle-down"></i></a>
              <ul class="dropdown-menu">
                                <li><a href="/registro">REGISTRATE E INGRESA CON TU USUARIO</a></li>
                                
                            </ul>
            </li>
      
            <li><a Style = "color: white" href="{{ asset('files/file2.pdf') }}" target="_blank">AYUDA</a></li>
                    </ul>
                </div>
            </div>
        </div>
  </header>
	<!-- end header -->
	<section id="featured" class="bg">
	<!-- start slider -->

			
	<!-- start slider -->
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
	<!-- Slider -->
        <div id="main-slider" class="main-slider flexslider">
            <ul class="slides">
              <li>
                <img src="img/slides/flexslider/1.jpg" alt="" />
                <div class="flex-caption">
                    <h3>Crea tu subasta</h3> 
					<p>Crea tu propia subasta. Asigna información adicional y escoge desde que precio empezaras a vender tu casa. </p> 
					
                </div>
              </li>
              <li>
                <img src="img/slides/flexslider/2.jpg" alt="" />
                <div class="flex-caption">
                    <h3>Participa en una subasta.</h3> 
					<p>Mira, participa y gana la casa de tus sueños subastando y siendo el mejor postor.</p> 
					
                </div>
              </li>
              <li>
                <img src="img/slides/flexslider/3.jpg" alt="" />
                <div class="flex-caption">
                    <h3>Perfil</h3> 
					<p>En tu perfil puedes ver la lista de casas que has ganado subastando y aquellas que quieres subastar,  como también, modificar y actualizar tus datos personales. </p> 
					
                </div>
              </li>
              <li>
              	<img src="img/slides/flexslider/4.jpg" alt="">
              	<div class="flex-caption">
              		<h3>Transacciones</h3> 
              		<p>Puedes iniciar una transacción con algún administrador en la pestaña <transacciones>, fácil y sencillo, para que empieces a subastar.</p> 
					
              	</div>
              </li>
              <li>
              	<img src="img/slides/flexslider/5.jpg" alt="">
              	<div class="flex-caption">
              		<h3>Contacto</h3> 
              		<p>Información de interés, dudas y preguntas podrán ser solucionadas con nuestros diversos medios de contactos.</p> 
					
              	</div>
              </li>
              <li>
              	<img src="img/slides/flexslider/6.jpg" alt="">
              	<div class="flex-caption">
              		<h3>¡Anímate a participar!</h3> 
              		<p>Topi-Subastas está hecha para ti, para que participes y ganes, con seguridad podrás ganar la casa que tanto has deseado.</p> 
					
              	</div>
              </li>
              <li>
              	<img src="img/slides/flexslider/7.jpg" alt="">
              	<div class="flex-caption">
              		<h3>Ayuda</h3> 
              		<p>Solicita la ayuda que te brindamos en nuestro sitio para mejorar tu experiencia usando TOPI-SUBASTAS.</p> 
              	</div>
              </li>
              <li>
              	<img src="img/slides/flexslider/8.jpg" alt="">
              	<div class="flex-caption">
              		
              	</div>
              </li>
              <li>
              	<img src="img/slides/flexslider/9.png" alt="">
              	<div class="flex-caption">
              		
              	</div>
              </li>
            </ul>
        </div>
	<!-- end slider -->
			</div>
		</div>
	</div>	


	</section>
	
	

	
	<footer Style = "background-color : white">
	
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				
					<div class="copyright">
						<p>&copy; Sailor Theme - All Right Reserved</p>
                        <div class="credits">
                            
                            <a href="https://bootstrapmade.com/free-business-bootstrap-themes-website-templates/">Business Bootstrap Themes</a> by <a href="https://bootstrapmade.com/">BootstrapMade</a>
						                     
					   </div>
						
					</div>
				 
				
			</div>
		</div>
	</div>
	</footer>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>

<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.3/angular-route.js"></script>
<script src="js/script2.js"></script>
<script src="js/dropzone.js"></script>
<script src="http://momentjs.com/downloads/moment.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="plugins/flexslider/jquery.flexslider-min.js"></script> 
<script src="plugins/flexslider/flexslider.config.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/stellar.js"></script>
<script src="js/classie.js"></script>
<script src="js/uisearch.js"></script>
<script src="js/jquery.cubeportfolio.min.js"></script>
<script src="js/google-code-prettify/prettify.js"></script>
<script src="js/animate.js"></script>
<script src="js/custom.js"></script>





	
</body>
</html>

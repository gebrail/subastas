<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Topi-Subastas</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Bootstrap 3 template for corporate business" />
<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/cubeportfolio.min.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />

<!-- Theme skin -->
<link id="t-colors" href="skins/default.css" rel="stylesheet" />

	<!-- boxed bg -->
	<link id="bodybg" href="bodybg/bg1.css" rel="stylesheet" type="text/css" />
 <script src="https://www.efecty.com.co/Content/js/jquery.js" type="text/javascript"></script>
 <script type="text/javascript">
    $(document).ready(function() {


                $("#monto").keyup(function() {                
                    var monto = $(this).val();
                    var valortotal = 0;
                    $("#msgerr").empty();

                     if (monto >0) {
                        $('#monto').tooltip('destroy');
                          x = parseInt(monto)/2900;
                          valortotal = x.toFixed(2);
                        $("#valorusd").val(valortotal);
                    }else {
                            if (monto == "") {
                                    $("#msgerr").empty();
                                }
                        $("#valorusd").val("");
                        $('#monto').tooltip({});
                        $("#monto").trigger('mouseenter');
                    }
                              });

            });

                    </script>
<!-- =======================================================
    Theme Name: Sailor
    Theme URL: https://bootstrapmade.com/sailor-free-bootstrap-theme/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
======================================================= -->
</head>
<body>


<div id="wrapper">
	<!-- start header -->
		<header>
			<div class="top">
				<div class="container">
					
				</div>
			</div>	
			
        <div class="navbar navbar-default navbar-static-top" style = "background-color:#DA0404">
            <div class="container" >
                <div class="navbar-header" >
                    <button   type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="img/LOGO_TOPICOS.png" alt="" width="76" height="50" /></a>
                </div>
                <div class="navbar-collapse collapse"  >
                    <ul class="nav navbar-nav">
                        <li ><a Style = "color: white" href="/">INICIO</a></li>
                        <li class="dropdown">
                            <a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">PERFIL  <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="/perfil">VER PERFIL</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">TU SUBASTA  <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="/registrarsubasta">CREA TU SUBASTA</a></li>
                                <li><a href="/terminadas">DECIDE EL DESTINO DE TU CASA</a></li>
								
                            </ul>
						</li>
                        <li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">SUBASTAR<i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="/subasta">PARTICIPA EN UNA SUBASTA</a></li>
                                
                            </ul>
						</li>
						<li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">TRANSACCIONES  <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="/gettransaccion">INICIAR UNA TRANSACCIÓN</a></li>
                                
                            </ul>
						</li>
                        <li><a Style = "color: white" href="/salir">Salir</a></li>
                    </ul>
                </div>
            </div>
        </div>
	</header>
	<!-- end header -->
	<section id="content">
		<div class="container">
				<div class="col-md-4">
  <form action="{{ url('/pagar') }}" enctype="multipart/form-data" method="POST" id="form">
  				   	{{ csrf_field() }} 

						<h3 align = "center"> Recarga de forma segura directamente a tu cuenta PayPal </h3>
				<input  type="number" id="monto" name="monto" class="form-control input-lg" placeholder="¿Cuánto deseas recargar?" tabindex="4" required> <br />
                 <input type="hidden" name="valor" id="valorusd" required="required"  class="form-control">
				<input Style = "background-color:#DA0404 ; opacity:0.7" type="submit" value="Recargar" class="btn btn-primary btn-block btn-lg" tabindex="7">
				</form>
			</div>
			<div class="col-md-8">
			<h1 align = "center"> Realiza las transacciones de tus casas ganadas de forma segura </h1>
			<table class="table table-striped">
        <thead>
            <th>
               Id subasta
            </th>
            <th>
                Id vendedor
            </th> <th>
               Id comprador
            </th>
            <th>
                valor final 
            </th>
             <th>
                Id Producto
            </th>
             <th>
                Fecha 
            </th>
                   <!--     <th>
                PAGAR (Realizar Transaccion)
            </th> -->
            <tbody>
                @foreach($transaccion as $s)
                <tr>
                  <td>
                         {{ $s->id_subasta}}
                    </td> 
                  <td>
                         {{ $s->id_vendedor}}
                    </td> 
                  <td>
                         {{ $s->id_comprador}}
                    </td> 
                    <td>
                         {{ $s->valor}}
                    </td> 
                    <td>
                        {{ $s->id_producto}}
                    </td>
                    <td>
                        {{ $s->fecha}}
                    </td>
                       
                       
                       <!-- <td>
                           <a class="btn btn-warning" href="/pagar/{{$s->id_producto}}/{{$s->valor}}/{{$s->id_subasta}}">
                            <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
                        </a>
                    </td>  -->                  
                </tr>
                @endforeach
            </tbody>
        </thead>
    </table>

			
		</div>		
		</div>
		
	</section>
	

	<footer Style = "background-color : white">
	
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				
					<div class="copyright">
						<p>&copy; Sailor Theme - All Right Reserved</p>
                        <div class="credits">
                            
                            <a href="https://bootstrapmade.com/free-business-bootstrap-themes-website-templates/">Business Bootstrap Themes</a> by <a href="https://bootstrapmade.com/">BootstrapMade</a>
						                     
					   </div>
						
					</div>
				 
				
			</div>
		</div>
	</div>
	</footer>
	
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/stellar.js"></script>
<script src="js/classie.js"></script>
<script src="js/uisearch.js"></script>
<script src="js/jquery.cubeportfolio.min.js"></script>
<script src="js/google-code-prettify/prettify.js"></script>
<script src="js/animate.js"></script>
<script src="js/custom.js"></script>
</body>
</html>

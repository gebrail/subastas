<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Topi-Subastas</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Bootstrap 3 template for corporate business" />
<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="plugins/flexslider/flexslider.css" rel="stylesheet" media="screen" />	
<link href="css/cubeportfolio.min.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />

<!-- Theme skin -->
<link id="t-colors" href="skins/default.css" rel="stylesheet" />

	<!-- boxed bg -->
	<link id="bodybg" href="bodybg/bg1.css" rel="stylesheet" type="text/css" />

<!-- =======================================================
    Theme Name: Sailor
    Theme URL: https://bootstrapmade.com/sailor-free-bootstrap-theme/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
======================================================= -->

</head>
<body >


<div id="wrapper">
	<!-- start header -->
		<header>
			<div class="top">
				<div class="container">
					
				</div>
			</div>	
			
        <div class="navbar navbar-default navbar-static-top" style = "background-color:#DA0404">
            <div class="container" >
                <div class="navbar-header" >
                    <button   type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="img/LOGO_TOPICOS.png" alt="" width="76" height="50" /></a>
                </div>
                <div class="navbar-collapse collapse"  >
                    <ul class="nav navbar-nav">
                        <li ><a Style = "color: white" href="/">INICIO</a></li>
                        <li class="dropdown">
                            <a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">PERFIL  <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="/perfil">VER PERFIL</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">TU SUBASTA  <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="/registrarsubasta">CREA TU SUBASTA</a></li>
                                <li><a href="/terminadas">DECIDE EL DESTINO DE TU CASA</a></li>
								
                            </ul>
						</li>
                        <li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">SUBASTAR<i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="/subasta">PARTICIPA EN UNA SUBASTA</a></li>
                                
                            </ul>
						</li>
						<li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">TRANSACCIONES  <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="/gettransaccion">INICIAR UNA TRANSACCIÓN</a></li>
                                
                            </ul>
						</li>
                        <li><a Style = "color: white" href="/salir">Salir</a></li>
                    </ul>
                </div>
            </div>
        </div>
	</header>
	<!-- end header -->
	
	
	<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				
						
			<div Style = "float:center" class="col-lg-6">
			     
        <br />
          <form action="/crear" method="post" enctype="multipart/form-data" >
    {{ csrf_field() }}
    
    <br />
	<input align= "center" class= "form-control input-lg" type="file" id="files" multiple name="files[]" />
		    <br /><br />

        <output id="list">

        </output>
		
        
        <script>




  function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = ['<img class="thumb"  src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('list').insertBefore(span, null);
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }

  document.getElementById('files').addEventListener('change', handleFileSelect, false); 
</script>
	
<!--	<input Style = "background-color:#DA0404 ; opacity:0.7" type="submit" value="Cargar imagenes" class="btn btn-primary btn-block btn-lg" tabindex="7">
-->
	</div>
	
	<div class="col-lg-6">
	
	</div>

				
				
			</div>
			<div class="col-lg-4">
			
				<aside class="right-sidebar">
				
				<h2 align = "center">Detalles de la subasta</h2>
				<br /> 
				<input  class= "form-control input-lg" type="text" placeholder= "Nombre" name="nombre" required="" />	<br />
				<textarea class= "form-control input-lg" type="text"  cols="10" rows="5" placeholder= " Descripcion" name="descripcion" required=""></textarea><br />
				<input class= "form-control input-lg" type="text" placeholder= "Ubicación" name="ubicacion" required="" />	<br />
				<input class= "form-control input-lg" type="number" placeholder= "Valor inicial ($)" name="valor"  required="" />	<br /> <br />
				<input Style = "background-color:#DA0404 ; opacity:0.7" type="submit" value="Enviar a la subasta" class="btn btn-primary btn-block btn-lg" tabindex="7" >
				   </form>

	
				</aside>

			</div>
			
		</div>
	</div>
	</section>
	<footer Style = "background-color : white">
	
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				
					<div class="copyright">
						<p>&copy; Sailor Theme - All Right Reserved</p>
                        <div class="credits">
                            
                            <a href="https://bootstrapmade.com/free-business-bootstrap-themes-website-templates/">Business Bootstrap Themes</a> by <a href="https://bootstrapmade.com/">BootstrapMade</a>
						                     
					   </div>
						
					</div>
				 
				
			</div>
		</div>
	</div>
	</footer>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="plugins/flexslider/jquery.flexslider-min.js"></script> 
<script src="plugins/flexslider/flexslider.config.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/stellar.js"></script>
<script src="js/classie.js"></script>
<script src="js/uisearch.js"></script>
<script src="js/jquery.cubeportfolio.min.js"></script>
<script src="js/google-code-prettify/prettify.js"></script>
<script src="js/animate.js"></script>
<script src="js/custom.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.3/angular-route.js"></script>
<script src="js/script2.js"></script>

	
</body>
</html>